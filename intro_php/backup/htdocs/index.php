<?php
require_once('db_inc.php');
connect();
require_once('layout.inc.php');
require_once('auth.inc.php');

if (array_key_exists('logout',$_REQUEST)) {
	if ($_REQUEST['logout']) {
		logout();
	}
}

myhead('Main Page',60);
echo "
<table>
    <tr>
        <td width='20'><a href='scores.php'><img src='images/scoreboard_icon.png' width='60' height='60' /></a></span></td>
        <td style='border-right:2px solid #f4f4f4;'><span style='font-size: 14px;'>The <a href='scores.php'>scoreboard</a></span></td>
        <td width='20'><a href='advisories.php?game=$GAMEID'><img src='images/notepad.png' width='60' height='60' /></a></td>
        <td><span style='font-size: 14px;'>Everything about <a href='advisories.php?game=$GAMEID'>advisories.</a></span></td>
    </tr>
    <tr>
        <td width='20'><a href='team_service.php'><img src='images/statistics-icon.png' width='60' height='60' /></a></td>
        <td style='border-right:2px solid #f4f4f4;'><span style='font-size: 14px;'>Statistics<br /><a href='team_service.php'>Which team compromised which service?</span></td>
        <td width='20'><a href='debugging_info.php'><img src='images/Information_icon.png' width='60' height='60' /></a></td>
        <td><span style='font-size: 14px;'>Verbose <a href='debugging_info.php'>Services' Status</a></span></td>
    </tr>
</table>";

echo "</br></br>";
echo '<table id="mytable" cellspacing="0" summary="Comments" align="center">
<th scope="row" class="spec"><span style="font-size: 18px;">Announcements </span>';
if (is_admin()) print " [ <a href='announcements.php'>New</a> ]";
echo'</th>
</table>';

$q = mysql_query("SELECT unix_timestamp(timestamp) as time,message FROM announce WHERE fi_game=$GAMEID ORDER BY timestamp DESC");
$count = 0;
while($row=mysql_fetch_array($q)) {
  if(!$count) echo '<table id="mytable" cellspacing="0" summary="Comments" align="center">';
  $msg_time = strftime("%d.%m. %H:%M",$row['time']);
  if ( $count % 2 == 0 )
	echo "<tr><th align=center nowrap width='120'>$msg_time</th><th>".$row['message']."</th></tr>";
  else
	echo "<tr><td align=center nowrap width='120'>$msg_time</td><td>".$row['message']."</td></tr>";
  ++$count;
}
if ($count) {
  echo '</table>';
} else {
  echo '<i>There are currently no announcements.</i>';
}

echo '<p>&nbsp;</p>';
myfooter();
?>
