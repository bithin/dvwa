<?php

$time_start = microtime (true);

function myhead ($title, $refresh = -1) {

	$pmenuBlocks = array();
	$pmenuBlocks[] = array( 'id' => 'Main Page', 'name' => 'Home', 'url' => '.' );
	$pmenuBlocks[] = array( 'id' => 'The rules', 'name' => 'Rules', 'url' => 'rules.php' );
	if (is_admin()) {
		$pmenuBlocks[] = array( 'id' => 'Admin Page', 'name' => 'Admin Home', 'url' => 'admin.php' );
		$pmenuBlocks[] = array( 'id' => 'logout', 'name' => 'Logout', 'url' => 'index.php?logout=1' );
	}

	$pmenuBlockHtml = '';
	foreach( $pmenuBlocks as $pmenuItem ) {
		$selectedClass = ( $pmenuItem[ 'id' ] == $title ) ? 'current' : '';
		$fixedUrl = $pmenuItem['url'];
		$pmenuBlockHtml .= "<li class=\"{$selectedClass}\"><a href=\"{$fixedUrl}\"><span>{$pmenuItem['name']}</span></a></li>";
	}

    /* Head section of HTML */
    echo "
        <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml'>
        <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
        <title>$title</title>";

    if ($refresh > 0) echo "<META HTTP-EQUIV='Refresh' CONTENT='$refresh'>\n  ";

    echo "
        <link rel='stylesheet' href='css/styles.css' type='text/css' />
        <link type='text/css' href='css/errors.css' rel='stylesheet' />
        </head>";
    /* Body Section */
    echo"
        <body>
        <div id='wrapper'>
        <div class='inner-wrapper'>";
    /* Header */
    echo"
        <div class='header clear'>
        <div class='subscribe'>
        <p>Final CTF Round</p>
        </div>
        <img src='images/inCTF-website-header-2011.jpg' style='padding-top:10px; padding-left: 10px;'/>
        </div>
        <div class='clear'></div>";
    /* Static Menu Section */
    echo"
        <div class='menu'>
        <ul class='clear'>". $pmenuBlockHtml ."
        <div align='right' style='margin:5px;margin-right:15px;'>
        <font face='verdana' color='white'>Howdy!</font>
        <div>
        </ul>
        </div>";
    echo"
        <div class='body clear'>
        <div class='content'>
        <div style='min-height: 400px;'>
        <h2>$title</h2>
        <hr><br />
        ";
}

function myfooter() {
    global $time_start;
    $time_end = microtime(true);

    echo"
        </div>
        </div>
        <div class='sidebar'>
        <ul>
        <li>
        <h4><a href='scores.php' style='text-decoration:none';>Scoreboard</a></h4>
        <ul>
            <li>Total Score</li>

        </ul>
        </li>
        <li>
        <h4><a href='advisories.php?game=1' style='text-decoration:none;'>Advisories</a></h4>
        <ul>
            <li>Everything about advisories.</li>
        </ul>

        </li>
        <li>
        <h4><a href='team_service.php' style='text-decoration:none;'>Teams on Services</a></h4>
        <ul>
            <li>Statistics: Which team compromised which service?</li>
        </ul>
        </li>

        <li>
        <h4><a href='debugging_info.php' style='text-decoration:none;'>Services' Status</a></h4>
        <ul>
            <li>Verbose Status of Services</li>
        </ul>
        </li>

    </ul>

        </div>
        </div>";
    echo"
        <div class='copyright'>
        <table border=0 width='100%'>
        <tr>
        <td align=left>
        Rendered in ".sprintf("%.5f",$time_end - $time_start)." seconds.
        </td>
        <td align=right>
        The official time is: ".strftime('%a, %d.%m.%y %H:%M:%S')."
        </td>
        </table>
        <p>&copy; 2011 InCTF </p>
        </div>
        </div>
        </div>";
    echo"
        </body>
        </html>
        ";
}

?>
