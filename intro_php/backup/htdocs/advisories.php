<?php
require_once('db_inc.php');
connect();
require_once('layout.inc.php');
require_once('auth.inc.php');

$teams = get_teams();
$services = get_services();
$services[0] = '(General Fault)';

$sql_filter='';
if (array_key_exists('service',$_GET)) {
  $serv_descr = param('service');
  if ($serv_descr != 'all') $sql_filter = 'WHERE fi_service = '.$serv_descr;
};

$sql = 'SELECT id, score, submittime, publishtime, fi_service, fi_team, judge, judgecomment FROM advisory '. $sql_filter .' ORDER BY submittime DESC;';

$result = query($sql);

$review = '';
if (is_admin()) $review='<td>Review</td>';

myhead('Advisories',60);

if (is_admin()) {
	echo "<p><font size='3'><a href='admin.php'>Back to the admin page.</a></p>";
} else {
	echo "<p><font size='3'><a href='.'>Back to the main page.</a></p>";
};

if (game_has_started()) {
echo "
<p><a href='advisory_submit.php'>Submit</a> a new advisory.</p>
<form method='get'/>
<p>Service <select name='service'><option value='all'>(View all)";
echo dict2options($services);
echo "</select><input type='submit' value='Filter By Service' /></p>


<table border=1 width='100%' id='tableTwo' class='yui'>
 <thead>
      <tr>
        <td class='tableHeader'>
          Advisories
        </td>
        <td colspan='8' class='filter'>


        </td>
      </tr>

<tr><th><a href='#' title='Click Header to Sort'>Service</a></th><th><a href='#' title='Click Header to Sort'>Team</a></th><th><a href='#' title='Click Header to Sort'>Time</a></th><th><a href='#' title='Click Header to Sort'>points</a></th><th width=20%><a href='#' title='Click Header to Sort'>Comment</a></th><th><a href='#' title='Click Header to Sort'>Display</a></th>$review </thead>
    <tbody></tr>";

  while ($row = mysql_fetch_array($result)) {
    if (!is_numeric($row['score'])) {
      $color='#ffcccc';
      $row['score'] = '?';
    } else {
      $color = 'white';
    }

    $time = date("H:i d.m.Y",$row["submittime"]);
    if(!$row['fi_service']) $row['fi_service']=0;
    echo "<tr>
      <td bgcolor='$color'>".$services[$row['fi_service']]."</td>
      <td bgcolor='$color'>".$teams[$row['fi_team']]."</td>
      <td bgcolor='$color'>".$time."</td>
      <td bgcolor='$color' align=right>".$row['score']."</td>";
    if ((is_numeric($row['score']) && isset($row['judge']) && isset($row['judgecomment']))) {
      if (strlen($row['judgecomment'])>0) {
        echo "<td bgcolor='$color'>".$row['judge'].' says &quot;'.$row['judgecomment']."&quot;</td>\n";
      } else {
        echo "<td bgcolor='$color'>".$row['judge']." says nothing.</td>\n";
      }
      if (!$row['publishtime']) {
        if ($row['judge']) {
          echo "<td bgcolor='$color'>(not to be published)</td>";
        } else {
          echo "<td bgcolor='$color'>(not reviewed)</td>";
        }
      } else {
        if (time() > $row['publishtime']) {
  	echo "<td bgcolor='$color'><a href='advisory_display.php?id=".$row['id']."'>Display</a></td>";
        } else {
  	echo "<td bgcolor='$color'>(not yet)</td>";
        }
      }
    } else {
        echo "<td bgcolor='$color'>&nbsp;</td><td bgcolor='$color'>(not reviewed)</td>";
    }
    if (is_admin()) {
            echo "<td bgcolor='$color'><a href='advisory_review.php?id=".$row['id']."'>Review</a></td>";
    } else {
    }
    echo "</tr>\n";
  }
 print "
  </tbody>
    <tfoot>
      <tr id='pagerTwo'>
        <td colspan='7'>
          <img src='img/first.png' class='first'/>
          <img src='img/prev.png' class='prev'/>
		  <input type='text' class='pagedisplay'/>
		  <img src='img/next.png' class='next'/>
          <img src='img/last.png' class='last'/>

          <select class='pagesize'>
            <option selected='selected'  value='5'>5</option>

            <option value='10'>10</option>

          </select>
        </td>
      </tr>
    </tfoot>";

echo "</table>
</form>";
} else {
echo "<p>Game has not started, yet</p>";
};

  myfooter();
// vim: et ts=2
?>


<!--******************************************JQUERY FOR TABLE *************************************************-->

    <script src="js/jquery-1.2.6.min.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter-2.0.3.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter.filer.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter.pager.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="themes/style1.css" media="screen">

    <script type="text/javascript">
        $(document).ready(function() {
            $("#tableOne").tablesorter({ debug: false, sortList: [[0, 0]], widgets: ['zebra'] })
                        .tablesorterPager({ container: $("#pagerOne"), positionFixed: false })
                        .tablesorterFilter({ filterContainer: $("#filterBoxOne"),
                            filterClearContainer: $("#filterClearOne"),
                            filterColumns: [0, 1, 2, 3 ,4, 5, 6],
                            filterCaseSensitive: false
                        });

            $("#tableTwo").tablesorter({ debug: false, sortList: [[2, 2]], widgets: ['zebra'] })
                .tablesorterPager({ container: $("#pagerTwo"), positionFixed: false })
                .tablesorterFilter({ filterContainer: $("#filterBoxTwo"),
                    filterClearContainer: $("#filterClearTwo"),
                    filterColumns: [0, 1, 2, 3, 4, 5, 6],
                    filterCaseSensitive: false
                });

            $("#tableTwo .header").click(function() {
                $("#tableTwo tfoot .first").click();
            });
        });


    </script>
<!--******************************************JQUERY FOR TABLE ENDS HERE *************************************************-->
