<?php
require_once('db_inc.php');
connect();
require_once('layout.inc.php');
require_once('auth.inc.php');

//myhead('Cross Flags',60);

$fresh_minutes = floor($displayfreshscores/60);

if (is_admin()) {
	echo "<p><font size='3'><a href='admin.php'>Back to the admin page.</a></p>";
} else {
	echo "<p><font size='3'><a href='.'>Back to the main page.</a></p>";
};


if (!($debug || game_has_started())) {

  echo "<center><p>The game has not started, please come back later.</p></center>";

} else {

  $fresh_now = time() - $displayfreshscores;
  $sum_total = 0;

  $teams = get_teams();
  $team_ids = array_keys($teams);
  sort($team_ids);

  foreach($team_ids as $id) {
    $sum_taken[$id] = 0;
    $sum_given[$id] = 0;
    foreach($team_ids as $id2) {
      $fresh[$id][$id2] = 0;
      $old[$id][$id2] = 0;
    }
  }

  $query = query('SELECT scores.fi_team as winner,`flag`.fi_team as looser,count(*) as nr,unix_timestamp(time) as time '.
                 'FROM scores,`flag` '.
                 "WHERE (scores.fi_game=$GAMEID)AND(`flag`.fi_game=$GAMEID)AND(scores.fi_flag=`flag`.id)".
                 "   AND(multiplier>0) GROUP BY winner,looser,(unix_timestamp(time)>=$fresh_now)");
  while($row = mysql_fetch_array($query)) {
    if ($row['time']>=$fresh_now) {
      $fresh[ $row['winner'] ][ $row['looser'] ] += $row['nr'];
    } else {
      $old[ $row['winner'] ][ $row['looser'] ] += $row['nr'];
    }
    $sum_taken[ $row['winner'] ] += $row['nr'];
    $sum_given[ $row['looser'] ] += $row['nr'];
    $sum_total += $row['nr'];
  }

  echo "<p>
        <table border=1 width='100%' id='tableTwo' class='yui'>
	<thead>	";
  echo "<tr><td class='tableHeader'>&nbsp;</td><td class='tableHeader'>Takers</td><td class='tableHeader' colspan=".(sizeof($teams)+2).">Givers</td></tr>
             <tr><th class=team>&nbsp;</th><th class=team><a href='#' title='Click Header to Sort'>Team Name</a></th>";
  foreach($team_ids as $id) print "<th class=team>$id</th>";
print "<th class=team><a href='#' title='Click Header to Sort'>SUM</a></th> </tr>\n  </thead>
    <tbody>";
  foreach($team_ids as $taker) {
    print "<tr><th class=team>$taker</th><th class=team>".$teams[$taker]."</th>";
    foreach($team_ids as $giver) {
      $fresh_ = $fresh[$taker][$giver];
      $old_   = $old[$taker][$giver];
      if(!$old_) $old_=0;
      if($fresh_>0) {
        print "<td class=score>$old_ + <font color=red>$fresh_</font></td>";
      } else {
        print "<td class=score>$old_</td>";
      }
    }
    print "<td class=sum>".$sum_taken[$taker]."</th></tr>\n";
  }
print "</tbody>
    <tfoot>
      <tr id='pagerTwo'>
        <td colspan='34'>
          <img src='img/first.png' class='first'/>
          <img src='img/prev.png' class='prev'/>
		  <input type='text' class='pagedisplay'/>
		  <img src='img/next.png' class='next'/>
          <img src='img/last.png' class='last'/>

          <select class='pagesize'>
            <option selected='selected'  value='5'>5</option>

            <option value='10'>10</option>

          </select>
        </td>
      </tr>
    </tfoot>";
  print "<tr><th>&nbsp;</th><th class=team style='color:#c80000'>SUM</th>";
  foreach($team_ids as $id) {
    print "<td class=sum>".$sum_given[$id]."</td>";
  }
  print "<td class=sum>".$sum_total."</td></tr></table>\n";

}

//myfooter();

?>
<!--******************************************JQUERY FOR TABLE *************************************************-->

    <script src="js/jquery-1.2.6.min.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter-2.0.3.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter.filer.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter.pager.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="themes/style1.css" media="screen">

    <script type="text/javascript">
        $(document).ready(function() {
            $("#tableOne").tablesorter({ debug: false, sortList: [[0, 0]], widgets: ['zebra'] })
                        .tablesorterPager({ container: $("#pagerOne"), positionFixed: false })
                        .tablesorterFilter({ filterContainer: $("#filterBoxOne"),
                            filterClearContainer: $("#filterClearOne"),
                            filterColumns: [0, 1, 2, 3 ,4, 5, 6],
                            filterCaseSensitive: false
                        });

            $("#tableTwo").tablesorter({ debug: false, sortList: [[0, 0]], widgets: ['zebra'] })
                .tablesorterPager({ container: $("#pagerTwo"), positionFixed: false })
                .tablesorterFilter({ filterContainer: $("#filterBoxTwo"),
                    filterClearContainer: $("#filterClearTwo"),
                    filterColumns: [0, 1, 2, 3, 4, 5, 6],
                    filterCaseSensitive: false
                });

            $("#tableTwo .header").click(function() {
                $("#tableTwo tfoot .first").click();
            });
        });


    </script>
<!--******************************************JQUERY FOR TABLE ENDS HERE *************************************************-->
