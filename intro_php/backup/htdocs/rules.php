<?php
require_once('layout.inc.php');
require_once('auth.inc.php');

myhead('The rules',3600);

if (is_admin()) {
	echo "<p><font size='2'><a href='admin.php'>Back to the admin page.</a></font></p>";
} else {
	echo "<p><font size='2'><a href='.'>Back to the main page.</a></font></p>";
};

echo"<p><font size='2'><ul>
";

function rule($title,$content) {
  return "<li><b>$title</b><br>
            $content
";
}

echo rule('General Rules:',
        '<ul>
        <li>Only ONE team per college campus will be considered for the prize;</li>
        <li>Prizes are awarded based on performance only;</li>
        </ul>');

echo rule('Advisories:',
        '<ul>
        <li>Teams are allowed to submit advisories during the game on any issue that they find within the vulnerable box;</li>
        <li>Advisories have to be submitted through a web form and are reviewed by the game\'s organisers;</li>
        <li>Advisories are published to all teams a certain amount of time after their submission (ranging from immediately to about 2-3 hours later - Best advisories are posted later);</li></ul>');

echo rule('Filtering:',
        '<ul>
        <li>Any kind of filtering that is not done in the applications themselves, or in a wrapper that is written during the game and handles only a single application, is considered against the rules;</li>
        <li>This specifically prohibts filtering based on IP addresses, other IP headers, TCP headers, ports, and the like;</li>
        <li>We also prohibit any kind of filtering or behaviour that tries to distinct between the gameserver and other players - while it remains allowed to distinct between an attack and a regular request;</li></ul>');

echo rule('Scoring:',
        'The total score of a team is calculated from three sub-categories
         <ul>'.
             rule('Ethical score/Advisories',
                  '<ul style="margin-bottom: -8px">'.
                  rule('Ethical behaviour',
                        'Each team is initially assigned 10 ethical points. Breaking the rules will result in deduction of the ethical points. <b>If you loose a total of 10 points then you WILL not be considered for prize and will effectively be out of the game</b>').
                  rule('Advisories',
                       'Each advisory is scored 0 to 5 ethical points, depending on its quality. We try to assign points/scores for each vulnerability only once, in a first come, first served fashion.').
                  '</ul>').
             rule('Defensive Score',
                  'A flag is considered defended and gets scored with 1 point, if and only if it was successfully retrieved by the gameserver and it wasn\'t submitted by another team by the end of its expiry time.').
             rule('Offensive Score',
                  'A flag is considered caught and gets awarded with 1 point, if and only if the submitting team has the same service actively running, of which the flag originates.').
             rule('Total',
             'The total score is calculated as follows: for each of the three categories a team is assigned a value of relative scores to the team with the most scores in each respective category. These two or three relative scores are then added and normalized, such that the leading team has 100%.')
        .'</ul>'
      );

echo rule('So, points are:',
        '<ul style="margin-bottom: -8px">'.
             rule('given for:',
                  '<ul style="margin-bottom: -8px">
                  <li>sending flags, captured from services of competitor teams (attack);</li>
                  <li>preventing access to a team\'s own flags by fixing vulnerabilities on provided servers and not affecting the functions performed by the vulnerable services (defence);</li>
                  <li>Submitting advisories: Points for advisories are awarded on first report basis;</li>
                  <li>Ensuring that the services are running at all times;</li>
                  </ul>').
             rule('withdrawn for:',
                  '<ul>
                  <li>affecting availability of a team\'s own services;</li>
                  <li>affecting the functions performed by the one own vulnerable services;</li>
                  <li>Teams submitting multiple copies of same advisories;</li>
                  <li><b>causing harmful actions like deleting files from other teams vulnerable image (e.g. rm -rf /). Please remember this is an application level ethical hacking contest. Only take advantage of the vulnerable applications(services) and capture flags.</b></li>
                  </ul>')
        .'</ul>'
      );

echo rule('During the game, teams are:',
        '<ul>'.
             rule('not allowed to:',
                  '<ul style="margin-bottom: -8px">
                  <li>Attack the game server or any other host of the organisers or admins;</li>
                  <li>Attack systems outside the VPN. All traffic has to happen within the VPN;</li>
                  <li>filter traffic to any CTF resources (e.g., by IP-addresses);</li>
                  <li>generate unreasonably large amounts of traffic (Flood);</li>
                  <li>In the IRC-channel: Swearing, flooding, and similar;</li>
                  </ul>').
             rule('teams are allowed to:',
                  '<ul style="margin-bottom: -8px">
                  <li>add any changes to the provided servers unless it is not explicitly prohibited by the admins;</li>
                  <li>conduct attacks against competitor teams\' servers to capture flags;</li>
                  </ul>').
             rule('This list is not complete;','')
        .'</ul>'
      );

echo rule('Finally, the admins can:',
        '<ul>
        <li>Specify/modify the rules at any point before the game begins;</li>
        <li>Penalize/disqualify a team for violation of the rules;</li>
        <li>Determine the winner on the basis of collected points;</li>
        <li>Revoke certificates of teams that cause unnecessary amount of traffic;</li>
        <li><b>This list is not complete;</b></li>
        </ul>'
      );

echo '
</ul></font></p>

<p>&nbsp;</p>';
myfooter();
?>
