<?php
// --------------------------------- clone of service_status.php
require_once('db_inc.php');
connect();
require_once('layout.inc.php');
require_once('auth.inc.php');
require_once('misc.inc.php');
require_once('status_codes.inc.php');

myhead('Verbose Status of Services',60);

if (is_admin()) {
	echo "<p><font size='3'><a href='admin.php'>Back to the admin page.</a></p>";
} else {
	echo "<p><font size='3'><a href='.'>Back to the main page.</a></p>";
};

$me = $_SERVER['PHP_SELF'];

if (!game_has_started())
	die("game has not started, yet");

$term = '';
if (isset($_REQUEST['term'])) $term=$_REQUEST['term'];
if(!(($term=='')||($term=='team')||($term=='service'))) {
	die("term '$term' not in whitelist");
}
$id  = get_int('id');

//************************************************** SELECT

echo "<table border=1 class='yui' >
	<tr><th colspan=2>Select filter</th></tr>";

$rows = query("SELECT id,name FROM service,game_x_service
               WHERE (service.id=game_x_service.fi_service)AND(game_x_service.fi_game=$GAMEID)
               ORDER BY service.id");
echo'<tr><th>Services</th><td> <form method=get>
		<input type=hidden name=term value=service>
		<select name=id>';
while($row = mysql_fetch_array($rows)) {
	$service[$row['id']] = $row['name'];
	$sel = '';
	if (($term=='service')&&($id==$row['id'])) $sel = 'selected';
	echo "<option $sel value=".$row['id'].'>'.$row['name'];
}
echo "</select><input type=submit value='OK'></form></td></tr>
      <tr><th>Teams</th><td> <form method=get>
		<input type=hidden name=term value=team>
		<select name=id>";
$rows = query("SELECT id,name FROM team,game_x_team
               WHERE (team.id=game_x_team.fi_team)AND(game_x_team.fi_game=$GAMEID)
               ORDER BY team.id");
while($row = mysql_fetch_array($rows)) {
	$team[$row['id']] = $row['name'];
	$sel = '';
	if (($term=='service')&&($id==$row['id'])) $sel = 'selected';
	echo "<option $sel value=".$row['id'].'>'.$row['name'];
}
echo "</select><input type=submit value='OK'></form></td></tr>\n";
echo "</table><hr>\n";

/*************************** DISPLAY DATA *********************************/

if (($id>0)&&($term!='')) {


$now = time();
if ($term == 'team') {
	$rows = $service;
	$cols = $team;
	$other = 'service';
} else {
	$rows = $team;
	$cols = $service;
	$other = 'team';
}
echo "<center><h3>$term: ".$cols[$id]."</h3></center>";


echo "<table width='100%' border=1 id='tableTwo' class='yui'>

 <thead>


	<tr><th><a href='#' title='Click Header to Sort'>$other</a></th><th><a href='#' title='Click Header to Sort'>Status</a></th><th><a href='#' title='Click Header to Sort'>Public Info</a></th></tr>  </thead>
    <tbody>";
reset($rows);
while(list($i,$name) = each($rows)) {
	echo "<tr><th style='width:25%' >$name</th>";
	$subq = query("SELECT info,status FROM service_status
                      WHERE (fi_game=$GAMEID)AND(fi_$other=$i)AND(fi_$term=$id)");
	if ($sub=mysql_fetch_array($subq)) {
		echo "<td style='width:25%'>".status($sub['status'])."</td>
                      <td><pre>".$sub['info']."</pre></td>";
	} else {
		echo "<td style='width:25%'>(no data yet)</td>";
	}
	echo "</tr>";
}

print "
  </tbody>
    <tfoot>
      <tr id='pagerTwo'>
        <td colspan='8'>
          <img src='img/first.png' class='first'/>
          <img src='img/prev.png' class='prev'/>
		  <input type='text' class='pagedisplay'/>
		  <img src='img/next.png' class='next'/>
          <img src='img/last.png' class='last'/>

          <select class='pagesize'>
            <option selected='selected'  value='5'>5</option>

            <option value='10'>10</option>

          </select>
        </td>
      </tr>
    </tfoot>";

echo "</table>";

} else {
	echo "<center>Choose something to display</center>";
}
myfooter();
?>

<!--******************************************JQUERY FOR TABLE *************************************************-->

    <script src="js/jquery-1.2.6.min.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter-2.0.3.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter.filer.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter.pager.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="themes/style1.css" media="screen">

    <script type="text/javascript">
        $(document).ready(function() {
            $("#tableOne").tablesorter({ debug: false, sortList: [[0, 0]], widgets: ['zebra'] })
                        .tablesorterPager({ container: $("#pagerOne"), positionFixed: false })
                        .tablesorterFilter({ filterContainer: $("#filterBoxOne"),
                            filterClearContainer: $("#filterClearOne"),
                            filterColumns: [0, 1, 2, 3 ,4, 5, 6],
                            filterCaseSensitive: false
                        });

            $("#tableTwo").tablesorter({ debug: false, sortList: [[0, 0]], widgets: ['zebra'] })
                .tablesorterPager({ container: $("#pagerTwo"), positionFixed: false })
                .tablesorterFilter({ filterContainer: $("#filterBoxTwo"),
                    filterClearContainer: $("#filterClearTwo"),
                    filterColumns: [0, 1, 2, 3, 4, 5, 6],
                    filterCaseSensitive: false
                });

            $("#tableTwo .header").click(function() {
                $("#tableTwo tfoot .first").click();
            });
        });


    </script>
<!--******************************************JQUERY FOR TABLE ENDS HERE *************************************************-->
