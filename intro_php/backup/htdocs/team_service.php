<?php
require_once('db_inc.php');
connect();
require_once('layout.inc.php');
require_once('auth.inc.php');

myhead('Teams on Services',60);

$fresh_minutes = floor($displayfreshscores/60);

if (is_admin()) {
	echo "<p><font size='3'><a href='admin.php'>Back to the admin page.</a></p>";
} else {
	echo "<p><font size='3'><a href='.'>Back to the main page.</a></p>";
};

echo "<p><font color=red>Red</font> numbers denote flags collected in the last $fresh_minutes minutes.</p>";

if (!($debug || game_has_started())) {

  echo "<center><p>The game has not started, please come back later.</p></center>";

} else {

  $fresh_now = time() - $displayfreshscores;
  $sum_total = 0;

  $teams = get_teams();
  $team_ids = array_keys($teams);
  sort($team_ids);

  $services = get_services();
  $service_ids = array_keys($services);
  sort($service_ids);

  $sum_total = 0;
  foreach($team_ids as $id) {
    $sum_team[$id] = 0;
    foreach($service_ids as $id2) {
      $sum_service[$id2] = 0;
      $fresh[$id][$id2] = 0;
      $old[$id][$id2] = 0;
    }
  }

  $fresh_now = time()-$displayfreshscores;
  $query = query("SELECT fi_team,fi_service,count(*) as nr,unix_timestamp(time) as time
                  FROM scores WHERE (scores.fi_game=$GAMEID)AND(multiplier>0)
                  GROUP BY fi_team,fi_service,(unix_timestamp(time)>=$fresh_now)");
  while($row = mysql_fetch_array($query)) {
    if ($row['time']>=$fresh_now) {
      $fresh[ $row['fi_team'] ][ $row['fi_service'] ] += $row['nr'];
    } else {
      $old[ $row['fi_team'] ][ $row['fi_service'] ] += $row['nr'];
    }
    $sum_team[ $row['fi_team'] ] += $row['nr'];
    $sum_service[ $row['fi_service'] ] += $row['nr'];
    $sum_total += $row['nr'];
  }

  echo "<p>
        <table border=1 width='100%' id='tableTwo' class='yui'>

		<thead>
      <tr>
        <td class='tableHeader'>
          Services
        </td>
        <td colspan='8' class='filter'>


        </td>
      </tr>



             <tr><th><a href='#' title='Click Header to Sort'>Team Name<a/></th>";
  foreach($service_ids as $id) print "<th class=team><a href='#' title='Click Header to Sort'>".$services[$id]."</a></th>";
  print "<th class=team><a href='#' title='Click Header to Sort'>SUM</a></th> </tr>\n  </thead>
    <tbody>";

  foreach($team_ids as $tid) {
    print "<tr><th style='color: #000'>".$teams[$tid]."</th>";
    foreach($service_ids as $sid) {
      $fresh1 = $fresh[$tid][$sid];
      $old1   = $old[$tid][$sid];
      if(!$old1) $old1=0;
      if($fresh1>0) {
        print "<td class=score>$old1 + <font color=red>$fresh1</font></td>";
      } else {
        print "<td class=score>$old1</td>";
      }
    }
    print "<td class=sum>".$sum_team[$tid]."</th></tr>\n";
  }


print "</tbody>
    <tfoot>
      <tr id='pagerTwo'>
        <td colspan='7'>
          <img src='img/first.png' class='first'/>
          <img src='img/prev.png' class='prev'/>
		  <input type='text' class='pagedisplay'/>
		  <img src='img/next.png' class='next'/>
          <img src='img/last.png' class='last'/>

          <select class='pagesize'>
            <option selected='selected'  value='5'>5</option>

            <option value='10'>10</option>

          </select>
        </td>
      </tr>
    </tfoot>";
    print "<tr><th class=team style='color:#c80000';>SUM</th>";
  foreach($service_ids as $id) {
    print "<td class=sum>".$sum_service[$id]."</td>";
  }
print "<td class=sum>".$sum_total."</td></tr>
  </table>\n";

}

myfooter();

?>
<!--******************************************JQUERY FOR TABLE *************************************************-->

    <script src="js/jquery-1.2.6.min.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter-2.0.3.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter.filer.js" type="text/javascript"></script>
    <script src="js/jquery.tablesorter.pager.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="themes/style1.css" media="screen">

    <script type="text/javascript">
        $(document).ready(function() {
            $("#tableOne").tablesorter({ debug: false, sortList: [[0, 0]], widgets: ['zebra'] })
                        .tablesorterPager({ container: $("#pagerOne"), positionFixed: false })
                        .tablesorterFilter({ filterContainer: $("#filterBoxOne"),
                            filterClearContainer: $("#filterClearOne"),
                            filterColumns: [0, 1, 2, 3 ,4, 5, 6],
                            filterCaseSensitive: false
                        });

            $("#tableTwo").tablesorter({ debug: false, sortList: [[0, 0]], widgets: ['zebra'] })
                .tablesorterPager({ container: $("#pagerTwo"), positionFixed: false })
                .tablesorterFilter({ filterContainer: $("#filterBoxTwo"),
                    filterClearContainer: $("#filterClearTwo"),
                    filterColumns: [0, 1, 2, 3, 4, 5, 6],
                    filterCaseSensitive: false
                });

            $("#tableTwo .header").click(function() {
                $("#tableTwo tfoot .first").click();
            });
        });


    </script>
<!--******************************************JQUERY FOR TABLE ENDS HERE *************************************************-->
